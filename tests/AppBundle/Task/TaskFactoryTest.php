<?php

namespace AppBundle\Tests\Task;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskList;
use AppBundle\Task\TaskFactory;
use PHPUnit\Framework\TestCase;

class TaskFactoryTest extends TestCase
{
    public function testCreateFromForm()
    {
        $task = new Task(
            "Tâche via constructeur",
            Task::DONE
        );

        $tasklist = new TaskList(
            "Liste pour avoir une TaskList dans TaskFactory::createFromForm"
        );

        $data = [
            "title" => "Via TaskFactory",
            "status" => Task::DONE
        ];

        $factory = new TaskFactory();
        $task2 = $factory->createFromForm($data, $tasklist);

        $this->assertInstanceOf(Task::class, $task2);
        $this->assertNotEquals($task->getTitle(), $task2->getTitle());
        $this->assertEquals($task->getStatus(), $task2->getStatus());

    }
}