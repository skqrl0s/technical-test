<?php

namespace AppBundle\Tests\Task;

use AppBundle\Entity\Task;
use AppBundle\Repository\TaskRepository;
use AppBundle\Task\TaskFactory;
use AppBundle\Task\TaskHandler;
use PHPUnit\Framework\TestCase;

class TaskHandlerTest extends TestCase
{
    public function testChangeStatus()
    {
        $task = new Task(
            "Tâche pour test",
            Task::TODO
        );

        $handler = new TaskHandler($this->getMockRepository(), $this->getMockFactory());

        $this->assertEquals(Task::TODO, $task->getStatus());
        $handler->handleChangeStatus($task);
        $this->assertEquals(Task::DONE, $task->getStatus());
        $handler->handleChangeStatus($task);
        $this->assertEquals(Task::TODO, $task->getStatus());
    }

    public function testEditAjax()
    {
        $task = new Task(
            "Tâche pour test",
            Task::TODO
        );

        $data["title"] = "Titre changé";

        $this->assertNotEquals($data["title"], $task->getTitle());

        $handler = new TaskHandler($this->getMockRepository(), $this->getMockFactory());

        $handler->handleEditAjax($task, $data);
        $this->assertEquals($data["title"], $task->getTitle());
    }

    public function getMockRepository()
    {
        return $this->getMockBuilder(TaskRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(["save"])
            ->getMock();
    }

    public function getMockFactory()
    {
        return $this->getMockBuilder(TaskFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}