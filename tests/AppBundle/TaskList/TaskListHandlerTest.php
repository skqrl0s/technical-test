<?php

namespace AppBundle\Tests\TaskList;

use AppBundle\Entity\TaskList;
use AppBundle\Repository\TaskListRepository;
use AppBundle\TaskList\TaskListFactory;
use AppBundle\TaskList\TaskListHandler;
use PHPUnit\Framework\TestCase;

class TaskListHandlerTest extends TestCase
{
    public function testEditAjax_with_correct_name()
    {
        $tasklist = new TaskList(
            "Liste de tâche"
        );

        $data["name"] = "Liste de tâche editée";

        $this->assertNotEquals($data["name"], $tasklist->getName());

        $handler = new TaskListHandler($this->getMockRepository(false), $this->getMockFactory());
        $handler->handleEditAjax($tasklist, $data);

        $this->assertEquals("Liste de tâche editée", $tasklist->getName());
    }

    public function testEditAjax_with_already_used_name()
    {
        $tasklist = new TaskList(
            "Liste de tâche"
        );

        $data["name"] = "Liste de tâche existant";

        $handler = new TaskListHandler($this->getMockRepository(true), $this->getMockFactory());
        $handler->handleEditAjax($tasklist, $data);

        $this->assertNotEquals("Liste de tâche existant", $tasklist->getName());
        $this->assertEquals("Liste de tâche", $tasklist->getName());
    }

    public function getMockRepository($return = NULL)
    {
        $mock = $this->getMockBuilder(TaskListRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(["save", "isNameAlreadyUsedString"])
            ->getMock();

        $mock->method("isNameAlreadyUsedString")->willReturn($return);

        return $mock;
    }

    public function getMockFactory()
    {
        return $this->getMockBuilder(TaskListFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
