<?php

namespace AppBundle\Tests\TaskList;

use AppBundle\Entity\TaskList;
use AppBundle\TaskList\TaskListFactory;
use PHPUnit\Framework\TestCase;

class TaskListFactoryTest extends TestCase
{
    public function testCreateFromForm()
    {
        $tasklist = new TaskList(
            "Liste de tâche instanciée via le constructeur"
        );

        $factory = new TaskListFactory();
        $tasklist2 = $factory->createFromForm([
            "name" => "Liste de tâche créée via TaskListFactory",
        ]);

        $this->assertInstanceOf(TaskList::class, $tasklist2);
        $this->assertNotEquals($tasklist->getName(), $tasklist2->getName());
    }
}