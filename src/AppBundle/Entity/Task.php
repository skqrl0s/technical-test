<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
 */
class Task
{
    const TODO = 0;
    const DONE = 1;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TaskList", inversedBy="tasks", fetch="EAGER")
     * @ORM\JoinColumn(name="tasklist_id", referencedColumnName="id")
     */
    private $tasklist;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    public function __construct(
        string $title,
        bool $status = Task::TODO,
        TaskList $taskList = null
    )
    {
        $this->title = $title;
        $this->tasklist = $taskList;
        $this->status = $status;
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getTaskListId()
    {
        return $this->tasklist->getId();
    }

    public function changeStatus()
    {
        if ($this->status == Task::TODO) {
            $this->status = Task::DONE;
        } else {
            $this->status = Task::TODO;
        }

        $this->updatedAt = new \DateTime('now');
    }

    public function selfEdit(array $data)
    {
        $this->title = $data['title'];
        $this->updatedAt = new \DateTime('now');
    }

}