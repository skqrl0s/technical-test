<?php

namespace AppBundle\Entity;

use AppBundle\TaskList\UniqueName;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TaskList
 *
 * @ORM\Table(name="task_list")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskListRepository")
 * @UniqueName()
 */
class TaskList
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Task", mappedBy="tasklist", fetch="EAGER", cascade={"remove"})
     */
    private $tasks;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    public function __construct(
        string $name
    )
    {
        $this->name = $name;
        $this->tasks = new ArrayCollection();
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @return mixed
     */
    public function getTasksCount()
    {
        return count($this->tasks);
    }

    public function selfEdit(array $data)
    {
        $this->name = $data['name'];
        $this->updatedAt = new \DateTime('now');
    }
}