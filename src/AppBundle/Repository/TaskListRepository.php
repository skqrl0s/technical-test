<?php

namespace AppBundle\Repository;

use AppBundle\Entity\TaskList;
use Doctrine\ORM\EntityRepository;

class TaskListRepository extends EntityRepository
{
    public function save(TaskList $taskList, $flush = true)
    {
        $this->_em->persist($taskList);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function delete(TaskList $taskList)
    {
        $this->_em->remove($taskList);
        $this->_em->flush();
    }

    public function isNameAlreadyUsedTaskList(TaskList $taskList)
    {
        $qb = $this->createQueryBuilder('tl');

        $result = $qb->where('tl.name = :name')
            ->setParameters([
                'name' => $taskList->getName(),
            ])
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function isNameAlreadyUsedString(string $name)
    {
        $qb = $this->createQueryBuilder('tl');

        $result = $qb->where('tl.name = :name')
            ->setParameters([
                'name' => $name,
            ])
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }
}