<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskList;
use Doctrine\ORM\EntityRepository;

class TaskRepository extends EntityRepository
{
    public function save(Task $task, $flush = true)
    {
        $this->_em->persist($task);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function delete(Task $task)
    {
        $this->_em->remove($task);
        $this->_em->flush();
    }

    public function deleteTasksDoneInTaskList(TaskList $taskList): int
    {
        $qb = $this->createQueryBuilder('t');

        $result = $qb->where('t.tasklist = :tasklist')
            ->andWhere('t.status = true')
            ->setParameters([
                'tasklist' => $taskList,
            ])
            ->getQuery()
            ->getResult();

        foreach ($result as $task) {
            $this->delete($task);
        }

        return count($result);
    }
}