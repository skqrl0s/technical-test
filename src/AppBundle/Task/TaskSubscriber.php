<?php

namespace AppBundle\Task;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TaskSubscriber implements EventSubscriberInterface
{
    private $session;

    public function __construct(
        SessionInterface $session
    )
    {
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            TaskEvents::TASK_STATUS_UPDATED => ['onTaskStatusUpdated'],
            TaskEvents::TASK_DELETED => ['onTaskDeleted'],
        ];
    }

    public function onTaskStatusUpdated(GenericEvent $event)
    {
        $task = $event->getSubject();
        $message = $task->getTitle() . ' : ' . ($task->getStatus() == 1 ? 'Fait' : 'A faire');

        $this->session->getFlashBag()->add(
            'notice',
            $message
        );
    }

    public function onTaskDeleted()
    {
        $this->session->getFlashBag()->add(
            'notice',
            'Tâche supprimée'
        );
    }
}
