<?php

namespace AppBundle\Task;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskList;
use AppBundle\Repository\TaskRepository;

class TaskHandler
{
    private $repository;
    private $factory;

    public function __construct(TaskRepository $taskRepository, TaskFactory $taskFactory)
    {
        $this->repository = $taskRepository;
        $this->factory = $taskFactory;
    }

    public function handleAddFromForm(array $data, TaskList $taskList)
    {
        $task = $this->factory->createFromForm($data, $taskList);

        $this->repository->save($task);
    }

    public function handleDelete(Task $task)
    {
        $this->repository->delete($task);
    }

    public function handleDeleteDoneTasksInList(TaskList $taskList): int
    {
        return $this->repository->deleteTasksDoneInTaskList($taskList);
    }

    public function handleChangeStatus(Task $task)
    {
        $task->changeStatus();
        $this->repository->save($task);
    }

    public function handleEditAjax(Task $task, array $data)
    {
        $task->selfEdit($data);
        $this->repository->save($task);
    }
}