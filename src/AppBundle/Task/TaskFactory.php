<?php

namespace AppBundle\Task;

use AppBundle\Entity\Task;
use AppBundle\Entity\TaskList;

class TaskFactory
{
    public function createFromForm(array $data, TaskList $taskList): Task
    {
        return new Task(
            $data['title'],
            array_key_exists('status', $data) ? $data['status'] : false,
            $taskList
        );
    }
}