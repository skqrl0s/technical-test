<?php

namespace AppBundle\Task;

final class TaskEvents
{
    const TASK_DELETED = 'task_deleted';
    const TASK_STATUS_UPDATED = 'task_status_updated';
}