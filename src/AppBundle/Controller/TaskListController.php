<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TaskList;
use AppBundle\Form\TaskListType;
use AppBundle\Form\TaskType;
use AppBundle\Task\TaskHandler;
use AppBundle\TaskList\TaskListEvents;
use AppBundle\TaskList\TaskListHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * TaskList Controller
 *
 * @Route("/tasklist")
 */
class TaskListController extends Controller
{
    /**
     * @var TaskListHandler
     */
    private $tasklistHandler;

    /**
     * @var TaskHandler
     */
    private $taskHandler;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * TaskListController constructor.
     * @param TaskListHandler $taskListHandler
     * @param TaskHandler $taskHandler
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(TaskListHandler $taskListHandler, TaskHandler $taskHandler, EventDispatcherInterface $eventDispatcher)
    {
        $this->tasklistHandler = $taskListHandler;
        $this->taskHandler = $taskHandler;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/", name="tasklist_index")
     * @Method("GET")
     */
    public function indexAction(): Response
    {
        $taskLists = $this->get('app.repository.tasklist')->findAll();

        return $this->render(':tasklist:index.html.twig', [
            'tasklists' => $taskLists,
        ]);
    }

    /**
     * @Route("/{id}", name="tasklist_show", requirements={"id":"\d+"})
     * @Method("GET")
     * @param TaskList $taskList
     * @return Response
     */
    public function showAction(TaskList $taskList): Response
    {
        return $this->render(':tasklist:show.html.twig', [
            'tasklist' => $taskList,
        ]);
    }

    /**
     * @Route("/add", name="tasklist_add")
     * @Method("GET|POST")
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request): Response
    {
        $form = $this->createForm(TaskListType::class);

        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $taskList = $this->tasklistHandler->handleAddFromForm($request->get('task_list'));

            $this->eventDispatcher->dispatch(TaskListEvents::TASKLIST_CREATED, new GenericEvent($taskList));

            return $this->redirectToRoute('tasklist_show', [
                'id' => $taskList->getId(),
            ]);
        }

        return $this->render(':tasklist:add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="tasklist_delete", requirements={"id":"\d+"})
     * @Method("GET")
     * @param TaskList $taskList
     * @return Response
     */
    public function deleteAction(TaskList $taskList): Response
    {
        $this->tasklistHandler->handleDelete($taskList);

        $this->eventDispatcher->dispatch(TaskListEvents::TASKLIST_DELETED, new GenericEvent($taskList));

        return $this->redirectToRoute('tasklist_index');
    }

    /**
     * @Route("/{id}/add-task", name="tasklist_add-task", requirements={"id":"\d+"})
     * @Method("GET|POST")
     * @param Request $request
     * @param TaskList $taskList
     * @return Response
     */
    public function addTaskAction(Request $request, TaskList $taskList): Response
    {
        $form = $this->createForm(TaskType::class);

        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $this->taskHandler->handleAddFromForm($request->get('task'), $taskList);
            $this->eventDispatcher->dispatch(TaskListEvents::TASKLIST_TASK_ADDED, new GenericEvent($taskList));

            return $this->redirectToRoute('tasklist_show', [
                'id' => $taskList->getId(),
            ]);
        }

        return $this->render(':tasklist:add-task.html.twig', [
            'tasklist' => $taskList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete-done-tasks", name="tasklist_delete-done-tasks", requirements={"id":"\d+"})
     * @Method("GET")
     * @param TaskList $taskList
     * @return Response
     */
    public function deleteDoneTasksAction(TaskList $taskList): Response
    {
        $numberOfDeletedTasks = $this->taskHandler->handleDeleteDoneTasksInList($taskList);

        $this->eventDispatcher->dispatch(TaskListEvents::TASKLIST_DELETE_DONE_TASKS, new GenericEvent($numberOfDeletedTasks));

        return $this->redirectToRoute('tasklist_show', [
            'id' => $taskList->getId(),
        ]);
    }

    /**
     * @Route("/{id}/edit-ajax", name="tasklist_edit_ajax", requirements={"id":"\d+"}, options={"expose" = true})
     * @Method("POST")
     * @param TaskList $taskList
     * @param Request $request
     * @return JsonResponse
     */
    public function editAjaxAction(TaskList $taskList, Request $request): JsonResponse
    {
        if ($request->isXmlHttpRequest()) {
            $data["name"] = $request->get('name');

            if ($taskList->getName() == $data["name"]) {
                return new JsonResponse([
                    "status" => 0,
                    "message" => "Nom identique",
                ]);
            }

            return new JsonResponse($this->tasklistHandler->handleEditAjax($taskList, $data));
        }
    }
}
