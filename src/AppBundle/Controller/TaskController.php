<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use AppBundle\Task\TaskEvents;
use AppBundle\Task\TaskHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Task Controller
 *
 * @Route("/task")
 */
class TaskController extends Controller
{
    /**
     * @var TaskHandler
     */
    private $taskHandler;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * TaskController constructor.
     * @param TaskHandler $taskHandler
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(TaskHandler $taskHandler, EventDispatcherInterface $eventDispatcher)
    {
        $this->taskHandler = $taskHandler;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/{id}/delete", name="task_delete", requirements={"id":"\d+"})
     * @Method("GET")
     * @param Task $task
     * @return Response
     */
    public function deleteAction(Task $task): Response
    {
        $this->taskHandler->handleDelete($task);

        $this->eventDispatcher->dispatch(TaskEvents::TASK_DELETED, new GenericEvent($task));

        return $this->redirectToRoute('tasklist_show', [
            'id' => $task->getTaskListId(),
        ]);
    }

    /**
     * @Route("/{id}/change-status", name="task_change_status", requirements={"id":"\d+"})
     * @Method("GET")
     * @param Task $task
     * @return Response
     */
    public function changeStatusAction(Task $task): Response
    {
        $this->taskHandler->handleChangeStatus($task);

        $this->eventDispatcher->dispatch(TaskEvents::TASK_STATUS_UPDATED, new GenericEvent($task));

        return $this->redirectToRoute('tasklist_show', [
            'id' => $task->getTaskListId(),
        ]);
    }

    /**
     * @Route("/{id}/edit-ajax", name="task_edit_ajax", requirements={"id":"\d+"}, options={"expose" = true})
     * @Method("POST")
     * @param Task $task
     * @param Request $request
     * @return Response
     * @internal param Request $requests
     */
    public function editAjaxAction(Task $task, Request $request): Response
    {
        $data["title"] = $request->get('title');

        if ($task->getTitle() != $request->get('title')) {
            $this->taskHandler->handleEditAjax($task, $data);

            return new Response('Modification enregistrée');
        }

        return new Response("Titre identique");
    }

}