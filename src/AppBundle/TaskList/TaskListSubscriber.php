<?php

namespace AppBundle\TaskList;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TaskListSubscriber implements EventSubscriberInterface
{
    private $session;

    public function __construct(
        SessionInterface $session
    )
    {
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            TaskListEvents::TASKLIST_CREATED => ['onTaskListCreated'],
            TaskListEvents::TASKLIST_DELETED => ['onTaskListDeleted'],
            TaskListEvents::TASKLIST_TASK_ADDED => ['onTaskListTaskAdded'],
            TaskListEvents::TASKLIST_DELETE_DONE_TASKS => ['onTasklistDeleteDoneTasks'],
        ];
    }

    public function onTaskListCreated()
    {
        $this->session->getFlashBag()->add(
            'notice',
            'La liste a été créée'
        );
    }

    public function onTaskListDeleted()
    {
        $this->session->getFlashBag()->add(
            'notice',
            'La liste a été supprimée'
        );
    }

    public function onTaskListTaskAdded()
    {
        $this->session->getFlashBag()->add(
            'notice',
            'Une tâche a été rajoutée dans la liste'
        );
    }

    public function onTasklistDeleteDoneTasks(GenericEvent $event)
    {
        $message = $event->getSubject() > 0 ? 'Les tâches faites ont été supprimées de la liste' : 'Aucune tâche à supprimer';

        $this->session->getFlashBag()->add(
            'notice',
            $message
        );
    }
}
