<?php

namespace AppBundle\TaskList;

use AppBundle\Repository\TaskListRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueNameValidator extends ConstraintValidator
{
    private $repository;

    public function __construct(TaskListRepository $taskListRepository)
    {
        $this->repository = $taskListRepository;
    }

    public function validate($object, Constraint $constraint)
    {
        if ($this->repository->isNameAlreadyUsedTaskList($object)) {
            $this->context->buildViolation('Attention, une liste avec le même nom existe déjà.')
                ->setTranslationDomain('unique_name')
                ->addViolation();

        }
    }
}