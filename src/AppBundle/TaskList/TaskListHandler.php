<?php

namespace AppBundle\TaskList;

use AppBundle\Entity\TaskList;
use AppBundle\Repository\TaskListRepository;
use Symfony\Component\HttpFoundation\Request;

class TaskListHandler
{
    private $repository;
    private $factory;

    public function __construct(TaskListRepository $taskListRepository, TaskListFactory $taskListFactory)
    {
        $this->repository = $taskListRepository;
        $this->factory = $taskListFactory;
    }

    public function handleAddFromForm(array $data): TaskList
    {
        $taskList = $this->factory->createFromForm($data);

        $this->repository->save($taskList);

        return $taskList;
    }

    public function handleDelete(TaskList $taskList)
    {
        $this->repository->delete($taskList);
    }

    public function handleEditAjax(TaskList $taskList, array $data): array
    {
        if ($this->repository->isNameAlreadyUsedString($data["name"])) {
            return [
                "status" => 0,
                "message" => "Une liste avec le nom renseigné existe déjà",
            ];
        } else {
            $taskList->selfEdit($data);
            $this->repository->save($taskList);

            return [
                "status" => 1,
                "message" => "Modification enregistrée",
            ];
        }
    }
}
