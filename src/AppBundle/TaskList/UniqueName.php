<?php

namespace AppBundle\TaskList;

use Symfony\Component\Validator\Constraint;

/** @Annotation */
class UniqueName extends Constraint
{
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}

