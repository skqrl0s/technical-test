<?php

namespace AppBundle\TaskList;

final class TaskListEvents
{
    const TASKLIST_CREATED = 'tasklist_created';
    const TASKLIST_TASK_ADDED = 'tasklist_task_added';
    const TASKLIST_DELETED = 'tasklist_deleted';
    const TASKLIST_DELETE_DONE_TASKS = 'tasklist_delete_done_tasks';
}
