<?php

namespace AppBundle\TaskList;

use AppBundle\Entity\TaskList;

class TaskListFactory
{
    public function createFromForm(array $data): TaskList
    {
        return new TaskList(
            $data['name']
        );
    }
}